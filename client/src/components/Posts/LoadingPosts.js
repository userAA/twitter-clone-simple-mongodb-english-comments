import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'

//the styles of module pages, characterizing detector of uploading posts
const styles = 
{
    load: 
    {
        textAlign: 'center',
        marginTop: 25,
        width: '100%'
    },
    loadIcon: 
    {
        color: '#8A2BE2'
    }
}

//the page of detector uploading posts
const LoadingPosts = (
{
    //the classes of styles of page detector uploading posts 
    classes 
}
) => 
{
    return (
        <div className={classes.load}>
            <CircularProgress className={classes.loadIcon}/>
        </div>
    )
}

//with help withStyles we linking style with props
export default withStyles(styles)(LoadingPosts)