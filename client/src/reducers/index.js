import { combineReducers } from "redux";
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import postReducer from './postReducer';
import profileReducer from './profileReducer';

//creating full reducer
export default combineReducers({
    //at output from reducer errorReducer state state.errors
    errors: errorReducer,   
    //at output from reducer authReducer state state.auth
    auth: authReducer,     
    //at output from reducer postReducer state state.auth 
    post: postReducer,     
    //at output from reducer profileReducer state state.profile
    profile: profileReducer 
})

