import {
    //finding the label of starting up detector of loading specified profile data
    LOAD_PROFILE,
    //finding label of request for receiving user profile data according to his specified ID userId
    GET_PROFILE  
} from '../constants'

//initial state
const initialState = {
    //the flag of uploading profile data of specified user (it equal false at initial time moment)
    loading: false,
    //itself profile data of specified user (it is empty at the initial moment of time)
    user: null 
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_PROFILE:
            return {
                ...state,
                //uploads profile data of specified user
                loading: true 
            }
        case GET_PROFILE:
            return {
                ...state,
                //profile data of specified user is successfully uploaded or did not upload 
                loading: false, 
                //the data profile of the specified user loaded from the server into the storage
                user: action.payload 
            }
        default:
            return state
    }
}

export default profileReducer;