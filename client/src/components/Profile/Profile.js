import React, {Component} from 'react'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import { connect } from "react-redux"
import 
{
    //we finding the function of uploading user posts according to it ID this.props.match.params.userId
    getPostsByUserId, 
    //we finding the function of user profile data according to it ID this.props.match.params.userId
    getUserProfile,   
    //we finding the function of filling arrays IDs from models data authorized user and user with ID this.props.match.params.userId
    followUser,   
    //we finding the function of clearing arrays IDs from models data authorized user and user with ID this.props.match.params.userId
    unfollowUser 
} from '../../actions/profileActions'

//we finding the page of separate posts
import Post from '../Posts/Post'

//we finding the page of detector uploading posts
import LoadingPosts from '../Posts/LoadingPosts'

//the page styles of conforming user profile with ID this.props.match.params.userId
const styles = 
{
    paper: 
    {
        padding: 10
    },
    email: 
    {
        color: '#888',
        marginBottom: 10
    },
    detailsBlock: 
    {
        display: 'flex'
    },
    detail: 
    {
        marginRight: 5,
        fontWeight: 'bold'
    },
    detailTitle: 
    {
        marginLeft: 1,
        textTransform: 'uppercase',
        fontSize: 10,
        fontWeight: 'normal'
    },
    btnBlock: 
    {
        width: '100%',
        textAlign: 'right'
    },
    btnFollow: 
    {
        backgroundColor: '#9400D3',
        color: 'white',
        '&:hover': 
        {
            color: '#9400D3',
            borderColor: '#9400D3',
            backgroundColor: 'white'
        }
    }
}

//the page of conforming user profile with ID this.props.match.params.userId
class Profile extends Component 
{
    constructor (props) 
    {
        super(props)

        this.handleFollow = this.handleFollow.bind(this)
        this.handleUnfollow = this.handleUnfollow.bind(this)
    }

    componentDidMount() 
    {
        //we downloading posts of specified user
        this.props.getPostsByUserId(this.props.match.params.userId)
        //we downloading profile of specified user data
        this.props.getUserProfile(this.props.match.params.userId)
    }

    componentDidUpdate(prevProps) 
    {
        if (this.props.auth.isAuthenticated) 
        {
            if (prevProps.user && prevProps.user.following !== this.props.user.following) 
            {
                this.props.getUserProfile(this.props.match.params.userId)
            } 
        }
    }

    handleFollow () 
    {
        //we connecting function followUser
        this.props.followUser(this.props.match.params.userId)
    }

    handleUnfollow () 
    {
        //we connecting function unfollowUser
        this.props.unfollowUser(this.props.match.params.userId)
    }

    render () 
    {
        const 
        {
            //classes of styles this pages
            classes, 
            //flag of detector of downloading list posts from server 
            loadingPosts, 
            //flag of detector of downloading profile of user data from server
            loadingProfile, 
            //the list of downloaded posts of user with ID this.props.match.params.userId 
            list,
            //block of authorizations
            auth, 
            //information about user with ID this.props.match.params.userId 
            user, 
            //profile user data with ID this.props.match.params.userId 
            profile 
        } = this.props

        let followBtns;
        //if there is an authorized user then we creating block of buttons of actions
        if (auth.isAuthenticated) 
        {
            if (user && user.following && user.following.indexOf(this.props.match.params.userId) === -1) 
            {
                //the button of connecting function followUser
                followBtns = (
                    <div className={classes.btnBlock}>
                        <Button 
                            variant="outlined" 
                            className={classes.btnFollow}
                            onClick={this.handleFollow}
                        >
                            Follow
                        </Button>
                    </div>
                )
            } 
            else 
            {
                //the button of connecting function unfollowUser
                followBtns = (
                    <div className={classes.btnBlock}>
                        <Button 
                            variant="outlined" 
                            className={classes.btnFollow}
                            onClick={this.handleUnfollow}
                        >
                            Unfollow
                        </Button>
                    </div>
                )
            }
        }

        let items;
        //we creating the list of specified posts
        items = list && list.map(el => <Post key={el._id} post={el}/>)

        //we creating information about profile data of specified user
        let profileInfo;
        if (profile && items) 
        {
            profileInfo = (
                <Paper className={classes.paper}>
                    <h1 className={classes.login}>{profile.login}</h1>
                    <div className={classes.email}>{profile.email}</div>
                    <div className={classes.detailsBlock}>
                        <div className={classes.detail}>
                            {/*The number of posts of the specified user*/}
                            {items.length}
                            <span className={classes.detailTitle}>posts</span>
                        </div>

                        <div className={classes.detail}>
                            {/*The number of IDs users in array IDs profile.followers */}
                            {profile.followers.length}
                            <span className={classes.detailTitle}>followers</span>
                        </div>
                        <div className={classes.detail}>
                            {/*The number of IDs users in array IDs profile.following */}
                            {profile.following.length}
                            <span className={classes.detailTitle}>following</span>
                        </div>

                        {followBtns}                                     
                    </div>
                </Paper>
            )
        }

        return (
            <div>
                {/*If flag of detector downloading of profile data of specified user is active then we showing 
                   itself detector overwise showing corresponding profile data*/}
                {loadingProfile ? <div>Loading</div> : profileInfo}
                {/*If flag of detector downloading of specified posts is active then we showing itself detector overwise 
                `` showing the list of downloaded posts*/}
                {loadingPosts ? <LoadingPosts /> :  items}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    //flag detector of downloading posts of user with ID this.props.match.params.userId 
    loadingPosts: state.post.loading,      
    //list of posts of user with ID this.props.match.params.userId 
    list: state.post.list,                 
    //profile data of user with ID this.props.match.params.userId 
    profile: state.profile.user,           
    //flag detector of downloading profile of user data with ID this.props.match.params.userId 
    loadingProfile: state.profile.loading, 
    auth: state.auth,                       
    user: state.auth.user                   
})


//functions getPostsByUserId and getUserProfile of downloading of posts and profile of user data according to it ID
// this.props.match.params.userId wraps in в props

//results state.post.loading, state.post.list, state.profile.user and state.profile.loading from store 
//from functions getPostsByUserId and getUserProfile come to mapStateToProps

//functions followUser and unfollowUser we wraping into props

//in mapStateToProps the results come from store from this functions in the form of state.auth and state.auth.user 

//with help withStyles we linking styles with props
export default connect(mapStateToProps, 
{
    getPostsByUserId, 
    getUserProfile, 
    followUser, 
    unfollowUser
})(withStyles(styles)(Profile));