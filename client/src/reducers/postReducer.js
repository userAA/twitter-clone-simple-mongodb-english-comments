import 
{
    //finding the label of request for adding new posts into storage store 
    ADD_POST, 
    //finding the label of carring out process of loading posts
    LOADING_POSTS, 
    //finding the label of request for receiving all posts from server
    GET_POSTS 
} from '../constants'


const initialState = 
{
    //at initial moment of time the storage of posts is empty
    list: null,    
    //the process of downloading posts from server at initial time moment is not being carried out
    loading: false 
}

const postReducer = (state = initialState, action) => 
{
    switch (action.type) 
    {
        //in store has added new post of authorized user
        case ADD_POST: 
            return {
                ...state,
                //new post of authorized user is added into storage store
                list: [action.payload, ...state.list] 
            }
        //the process of downloading posts from the server
        case LOADING_POSTS:
            return {
                ...state,
                //the process of downloading posts from the server
                loading: true
            }
        //received all specified posts and located them into store
        case GET_POSTS:
            return {
                ...state,
                //the process of downloading posts from server finished
                loading: false,
                //the list of all specified downloaded posts from server
                list: action.payload 
            }     
        default: 
            return state
    }
}

export default postReducer;