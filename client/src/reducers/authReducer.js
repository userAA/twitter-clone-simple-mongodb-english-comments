import {
    //finding label of sending into storage store the data about authorized user
    SET_CURRENT_USER,
    //label of adding user IDs
    FOLLOW,          
    //label of removing user IDs
    UNFOLLOW          
} from '../constants'

const initialState = 
{
    //the flag of authorization user in storage store
    isAuthenticated: false,
    //the information about authorized user in storage store
    user: null             
}

const authReducer = (state = initialState, action) => 
{
    switch (action.type) 
    {
        case SET_CURRENT_USER:
            return {
                ...state,
                // the flag of authorization user
                isAuthenticated: action.payload ? (Object.keys(action.payload).length !== 0) : false, 
                //infomation about authorized user, if it is not there, it means, that or user not authorized
                //or there is no registered user
                user: action.payload
            }
        case FOLLOW: 
            return {
                ...state,
                user: {
                    ...state.user,
                    //array of user IDs state.user.following we supplementing by user ID action.payload
                    following: [...state.user.following, action.payload] 
                }
            }
        case UNFOLLOW: 
            return {
                ...state,
                user: {
                    ...state.user,
                    //from array users IDs state.user.following we removing user ID action.payload
                    following: state.user.following.filter(item => item !== action.payload)
                }
            }
        default:
            return state;
    }
}

export default authReducer;