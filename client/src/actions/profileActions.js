import axios from 'axios'
import 
{
    //finding label of request for receiving of profile user data according to it specified ID userId
    GET_PROFILE, 
    //finding the label of starting up detector of loading specified profile data
    LOAD_PROFILE,
    //finding the label of request for receiving of all posts from server
    GET_POSTS,   
    //finding label of starting up of detector loading posts
    LOADING_POSTS, 
    FOLLOW, 
    UNFOLLOW
} 
from '../constants'

//the function of request for getting profile user data according to it specified ID userId
export const getUserProfile = (userId) => dispatch => 
{
    //starting up the detector of receiving profile of specified user from server
    dispatch(loadProfile())
    //we carrying out the get request for receiving profile of specified user from server
    axios.get(`http://localhost:3001/api/users/${userId}`).then(res => dispatch(
    {
        type: GET_PROFILE,
        //the required data profile is received and it is being uploaded to the store
        payload: res.data 
    }))
    .catch(err => console.log(err))
}

//the function of request for loading posts of user with ID userId from server
export const getPostsByUserId = (userId) => dispatch => 
{
    //starting up the detector of loading specified posts from server
    dispatch(loadPosts())
    //creating get request for loading of user posts with ID userId from server
    axios.get(`http://localhost:3001/api/posts/${userId}`).then(res => dispatch(
    {
        type: GET_POSTS,
        //the needed posts with server are loaded, they are transferred into the storage
        payload: res.data 
    }))
}

//the function of filling IDs arrays  from models data of authorized user and user with ID userId
export const followUser = (userId) => dispatch => 
{
    //request for adding user ID userId and authorized user into arrays of models corresponding users
    axios.post('http://localhost:3001/api/users/follow', {userId}).then(res => 
    //process passed succesfully
    dispatch(
    {
        type: FOLLOW,
        //user ID, which we will upload to the store
        payload: res.data.userId 
    }))
    .catch(err => console.log(err))
}

//the function of clearing of arrays of IDs from models data authorized user and user with ID userId 
export const unfollowUser = (userId) => dispatch => 
{
    //request for removing user ID iserId and authorized user from arrays models of according users
    axios.post('http://localhost:3001/api/users/unfollow', {userId}).then(res => 
    //process passed succesfully
    dispatch(
    {
        type: UNFOLLOW,
        //user ID, which we will unload from the storage store
        payload: res.data.userId 
    }))
    .catch(err => console.log(err))
}

//the search function of user data profile (including it posts) according to it specified login
export const searchUser = (searchData, history) => dispatch => 
{
    //making up post request for search user data profile (including it posts) according to it specified login
    axios.post('http://localhost:3001/api/users/search', searchData).then(res => 
    {
        //user data profile (including it posts) according to it specified login is found
        //moving into page of conforming user data profile (including it posts) according to it defined ID res.data.userId
        history.push(`/profile/${res.data.userId}`)
    })
    //didn't work out post request for search of user data profile (including it posts) according to it specified login
    .catch(err => history.push('/search'))
}

//the function of starting up of detector of receiving profile of specified user from server
export const loadProfile = () => 
{
    return {
        type: LOAD_PROFILE
    }
}

//the function od starting up of detector specified posts from server
export const loadPosts = () => 
{
    return {
        type: LOADING_POSTS
    }
}