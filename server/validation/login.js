const Validator = require('validator');

//the function of checking data of registrated user, needed for his authoruzation
module.exports = function(data) 
{
    let errors = {};

    //we checking email of registrated user
    if (Validator.isEmpty(data.email)) 
    {
        //there is no email of registrated user
        errors.email = 'Email field is required';
    }

    if (!Validator.isEmail(data.email)) 
    {
        //the email of registrated user is not correct
        errors.email = 'Email is invalid';
    }

    //we checking password of registrated user
    if (Validator.isEmpty(data.password)) 
    {
        //there is no password of registrated user
        errors.password = 'Password field is required';
    }

    if (!Validator.isLength(data.password, {min: 6, max: 30})) 
    {
        //the password of registrated user is not correct
        errors.password = 'Password must between 6 and 30 characters';
    }    

    return {
        errors,
        //the condition when user data of registered user, needed for his authorization correct
        isValid: Object.keys(errors).length == 0
    }
}