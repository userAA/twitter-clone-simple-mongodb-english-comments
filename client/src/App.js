import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import { Provider } from 'react-redux';
import jwt_decode from 'jwt-decode';

import store from './store';

import Main from './components/Layout/Main';

import Home from "./components/Home";
import Register from "./components/Auth/Register";
import Login from "./components/Auth/Login";
import Profile from "./components/Profile/Profile";
import NotFound from './components/Search/NotFound';

import setAuthHeader from './utils/setAuthHeader';
import {logoutUser, getCurrentUser} from './actions/authActions';

if (localStorage.getItem('jwtToken')) 
{
  const currentTime = Date.now() / 1000;
  const decode = jwt_decode(localStorage.getItem('jwtToken'))

  if (currentTime > decode.exp) 
  {
    //there is no authorized user
    store.dispatch(logoutUser())
    window.location.href = '/';
  } 
  else 
  {
    //there is authorized user
    setAuthHeader(localStorage.getItem('jwtToken'))
    //the information about authorized user we transmiting to store
    store.dispatch(getCurrentUser())
  }
}

class App extends Component {
  render () {
    return (
      //we fixing storage store
      <Provider store={store}>
        <div>
          <BrowserRouter>
            <Main>
              <Switch>
                {/*General page Home */}
                <Route exact path="/" component={Home}/>
                {/*Authorization page of registered user*/}
                <Route path="/login" component={Login}/>
                {/*Page user registration */}
                <Route path="/register" component={Register}/>
                {/*The page of user posts with corresponding ID */}
                <Route path="/profile/:userId" component={Profile}/>
                {/*The page, talking about that known user did not find */}
                <Route path="/not_found_user" component={NotFound}/>
              </Switch>
            </Main>
          </BrowserRouter>      
        </div>
      </Provider>
    );
  }
}

export default App;