import axios from "axios"
import 
{
    //finding the label of request for adding new post into storage store
    ADD_POST, 
    //finding the label of request for adding all posts from server 
    GET_POSTS,
    //finding the label of detector starting up of loading posts
    LOADING_POSTS 
} from '../constants'

//the function of request for creating new post of authorized user 
export const addPost = postData => dispatch => 
{
    //post request for creating new post of authorized user
    axios.post('http://localhost:3001/api/posts/add', postData).then(res => dispatch(
    {
        type: ADD_POST,
        //new post of authorized user was created, res.data - it data
        //this post we sending into storage store
        payload: res.data
    }))
    .catch(err => console.log(err))
}

//the function of request for receiving of all posts from server  
export const getPosts = () => dispatch => 
{
    //we starting up the detector of loading specified posts from server
    dispatch(loadPosts)
    //get request for receiving of all posts from server
    axios.get('http://localhost:3001/api/posts').then(res => dispatch(
    {
        type: GET_POSTS,
        //received all posts from server, them we sending into storage store
        payload: res.data
    }))
    .catch(err => console.log(err))
}

//the function of the request to receive posts whose user IDs
//correspond to the user ids of the following array of the database model of the authorized user on the server
export const getPostsByFolowingUsers = () => dispatch => 
{
    //we starting up the detector of loading specified posts from server
    dispatch(loadPosts)
    axios.get('http://localhost:3001/api/posts/following').then(res => dispatch(
    {
        type: GET_POSTS,
        //received needed posts from server, them we sending into storage store
        payload: res.data
    }))
    .catch(err => console.log(err))
}

//the function of starting up of loading detector of specified posts from server
export const loadPosts = () => 
{
    return {
        type: LOADING_POSTS
    }
}