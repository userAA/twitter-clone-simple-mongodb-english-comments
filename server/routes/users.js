const router = require('express').Router()

//finding the model of database of user User
const User = require('../models/User') 

const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const passport = require("passport")

//finding the function of check data according user registration
const validateRegisterInput = require("../validation/register")

//finding the function of check data according to authorization user
const validateLoginInput = require("../validation/login")

//router of user registration
router.route('/register').post((req,res) => 
{
    //we checking data according to user registration
    const {isValid, errors} = validateRegisterInput(req.body);

    //the data according to user registration are not correct we returning error
    if (!isValid) 
    {
        return res.status(404).json(errors);
    }

    //we checking if there is a user with the entered email
    User.findOne(
    {
        email: req.body.email
    })
    .then(user => 
    {
        //if there is then we returning error
        if (user) 
        {
            errors.email = 'Email was used!'
            return res.status(404).json(errors);
        }

        //if there is no that we registering new user in model User on server mongodb
        //at this we encrypting the password of the new registered user
        bcrypt.genSalt(10, function(err, salt)
        {
            bcrypt.hash(req.body.password, salt, function(err, hash) 
            {
                const newUser = new User(
                {
                    email: req.body.email,
                    login: req.body.login,
                    password: hash
                })
                newUser.save().then(newUser => res.json(newUser)).catch(err => console.log(err))
            })
        })
    })
})

//authorized registered user router
router.route('/login').post((req,res) => 
{
    //we checking data according to authorization of registered user
    const {errors, isValid} = validateLoginInput(req.body);
        
    //data according to authorization of registered user is not correct
    if (!isValid) 
    {
        return res.status(404).json(errors);
    }

    //we checking if there is a user with the entered email
    User.findOne(
    {
        email: req.body.email
    })
    .then(user => 
    {
        //if there is so we authorizing such user, he in any case already is registered
        if (user) 
        {
            //we checking the password match
            bcrypt.compare(req.body.password, user.password).then(isMatch => 
            {
                if (isMatch) 
                {
                    //in the case of match we saving in token ID of authorized user
                    const token = jwt.sign({id: user._id}, process.env.SECRET, {expiresIn: "1d"}, function(err,token)
                    {
                        //created token of authorizing user we sending into frontend
                        //authorization of corresponding registered user succesfully finished
                        return res.json(
                        {
                            success: true,
                            token: token
                        })
                    })
                } 
                else 
                {
                    //overwise we giving the error passwords do not match
                    errors.password = "Password is incorrect"
                    return res.status(404).json(errors);
                } 
            })
        } 
        else 
        {
            //overwise we giving the error, there is no such user in model base User 
            errors.email = 'User not found';
            return res.status(404).json(errors);
        }
    })
})

//router of receiving data of authorized user via password
router.route('/').get( passport.authenticate('jwt', {session: false}), (req,res) => 
{
    //collecting all received data according to authorized user
    res.json(
    {
        _id: req.user._id,
        email: req.user.email,
        login: req.user.login,
        followers: req.user.followers,
        following: req.user.following,
    })
})

//router of fulling arrays following[] and followers[] of model database User on server MongoDb
router.route('/follow').post(passport.authenticate('jwt', {session: false}),(req, res) => 
{
    //req.user._id - received ID of authorized user from passport
    User.findOneAndUpdate(
    {
        _id: req.user._id        
    }
    , 
    {
        //in array following from user database model with ID req.user._id we added ID req.body.userId
        $push: 
        {
            following: req.body.userId
        } 
    }
    ,        
    {
        new: true
    })
    .then(user => 
    {
        User.findOneAndUpdate(
        {
            _id: req.body.userId        
        }
        , 
        {
            //in array followers from user database model with ID req.body.userId we added ID req.user._id
            $push: 
            {
                followers: req.user._id
            }
        }
        , 
        {
            new: true
        })
        .then(user =>
        {
            res.json(
            {
                userId: req.body.userId
            })
        }
        )
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))
})

//router of clearing arrays following[] and followers[] of user database model on server MongoDb
router.route('/unfollow').post(passport.authenticate('jwt', {session: false}),(req, res) => 
{
    User.findOneAndUpdate(
    {
        _id: req.user.id
    }
    , 
    {
        //from array following of user database model with ID req.user._id we removed ID req.body.userId
        $pull : 
        {
            following: req.body.userId
        }
    }, 
    {
        new: true
    })
    .then(user => 
    {
        User.findOneAndUpdate(
        {
            _id: req.body.userId
        }
        , 
        {
            //from array followers of user database model with ID req.body.userId we removed ID req.user._id
            $pull: 
            {
                followers: req.user._id
            }
        }
        , 
        {
            new: true
        })
        .then(user => res.json(
        {
            userId: req.body.userId
        }))
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))      
})

//router of searching user data (including his possible posts) according to entering login or email
router.route('/search').post((req,res) => 
{
    //we seeking user (including his possible posts) in user database model on server MongoDb according to specified
    //req.body.text, which relevants or to his email or to his login 
    User.findOne(
    {
        $or: 
        [
            {email: req.body.text},
            {login: req.body.text}
        ]
    })
    //required user is found with his data (including all his possible posts) we sending into frontend his ID
    .then(user =>res.json(
    {
        userId: user._id
    }))
    //required user is not found
    .catch(err => res.status(404).json(
    {
        msg: 'User not found'
    }))
})

//router of receiving user data profile according to his specified ID req.params.id 
router.route('/:id').get((req,res) => 
{
    //we checking if there is a user with the specified ID in the user database model on the MongoDb server
    User.findById(req.params.id).then(user => 
    {
        //if there is
        if (user) 
        {
            //so create profile of specified user and we sending his into frontend
            return res.json(
            {
                _id: user._id,
                email: user.email,
                login: user.login,
                followers: user.followers,
                following: user.following 
            })
        } 
        else 
        {
            //otherwise, the required user does not exist in the corresponding database model
            return res.status(404).json({msg: "User not found"})
        }
    })
    .catch(err => console.log(err))
})

module.exports = router;