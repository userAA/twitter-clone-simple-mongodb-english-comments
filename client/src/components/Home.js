import React, {Component} from "react";
import {connect} from "react-redux";

import ListPost from './Posts/ListPost';
import Login from './Auth/Login';

class Home extends Component {
    render () {
        //we find out whether there is an authorized user or not
        const  { isAuthenticated }  = this.props;
        
        return (
            <div>
                {/*If there is an authorized user then we going to on page of list posts 
                   ListPost, which includes in itself the posts of authorized user,
                   overwise go to page Login - user authorization */}
                {isAuthenticated ? <ListPost/> : <Login/>}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    //we receiving from store флаг, indicating that there is authorized user 
    isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps)(Home);