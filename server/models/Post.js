const mongoose = require('mongoose')
const Schema = mongoose.Schema

//we creating database model according to posts
const postSchema = new Schema(
{
    //the user ID and login of the user who created the post
    user: 
    {
        type: Schema.Types.Object,
        required: true
    },
    //text of post
    text: 
    {
        type: String,
        required: true
    },
    //the time of creating post
    createdAt: 
    {
        type: Date,
        default: Date.now
    }
})

//we fixing database model according to posts in mongodb service
module.exports = mongoose.model('Post', postSchema);