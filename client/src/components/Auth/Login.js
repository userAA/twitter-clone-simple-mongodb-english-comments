import React, {Component} from "react";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux"

//finding  function request for authorization of a registered user from "../../actions/authActions"
import {loginUser} from "../../actions/authActions";

//the styles of authorization page of the registered user, they are via props going to classes
const styles = 
{
    textField: 
    {
        width: '100%',
        marginBottom: 5
    },
    btnBlock: 
    {
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 20
    }
}

//the class of page authorization of the registered user,
class Login extends Component 
{
    constructor (props) 
    {
        super(props);
        //the state of data already of a registered user, needed to carry out its authorization 
        this.state = 
        {
            email: '', 
            password: '',
            errors: {}
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount () 
    {
        //we moving into initial page if authorization is carried out succesfully
        if (this.props.auth.isAuthenticated) 
        {
            this.props.history.push('/')
        }
    }

    componentWillReceiveProps(nextProps) 
    {
        //authorization of registered user failed
        if (nextProps.errors) 
        {
            this.setState({errors: nextProps.errors})
        }

        //we moving into initial page if authorization is carried out succesfully
        if (nextProps.auth.isAuthenticated) 
        {
            nextProps.history.push('/')
        }
    }

    handleChange (e) 
    {
        //we collecting data about authorization of registered user
        this.setState({[e.target.name]: e.target.value});
    }

    handleSubmit (e) 
    {
        e.preventDefault();
        //we collecting data needed for authorization of registered user
        const userData = 
        {
            email: this.state.email,
            password: this.state.password
        }
        
        //we making up request for authorization of registered user
        this.props.loginUser(userData)
    }

    render () 
    {
        //finding from props the classes of styles of page user authorization
        const {classes} = this.props; 
        const {errors}  = this.state;
        return (
            <Paper style={{padding: 15}}>
                <form onSubmit={this.handleSubmit}>
                    {/*The email input field of the registered user we are authorizing  */}
                    <TextField
                        type="email"
                        label="Email"
                        name="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        className={classes.textField}
                        helperText={errors.email ? errors.email : ''}
                        error={errors.email ? true : false}
                    />
                    {/*The password input field of the registered user we are authorizing  */}
                    <TextField
                        label="Password"
                        type="password"
                        name="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        className={classes.textField}
                        helperText={errors.password ? errors.password : ''}
                        error={errors.password ? true : false}
                    />
                    <div className={classes.btnBlock}>
                        {/*The button of authorization of a registered user according to it email and password */}
                        <Button variant="outlined" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </Paper>
        )
    }
}

const mapStateToProps = (state) => 
({
    //the state of authorization of a registered user
    auth: state.auth,    
    //the errors which arised at authorization of a registered user is being appeared from errorReducer 
    errors: state.errors,
})

//the request function for authorization of a registered user loginUser we wraping into props
//at output we receiving the state in mapStateToProps and request for register user this.props.loginUser
//with help withStyles we relating styles with props
export default connect(mapStateToProps, {loginUser})(withRouter(withStyles(styles)(Login)));