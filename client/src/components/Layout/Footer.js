import React from 'react'
import MadeWithLove from 'react-made-with-love'
import {withStyles} from '@material-ui/core/styles'

//style of module of page refferene into github, it via props transmiting into classes 
const styles = 
{
    root:
    {
        textAlign: 'center',
        marginTop: 20
    }
}

//page refference into github
const Footer = (
{
    //the classes of styles of page refferense into github
    classes 
}) => 
{
    return (
        <div className={classes.root}>
            {/*Go to github */}
            <MadeWithLove
                by="Ngo Huong"
                emoji
                link="https://github.com"
            />
        </div>
    )
}

//with help withStyles we accociating style with props
export default withStyles(styles)(Footer);