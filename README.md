gitlab page https://gitlab.com/userAA/twitter-clone-simple-mongodb-english-comments.git
gitlab comment twitter-clone-simple-mongodb-english-comments

project twitter-clone-simple-mongodb
technologies used on the frontend:
    @material-ui/core,
    @material-ui/icons,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    jwt-decode,
    react,
    react-dom,
    react-made-with-love,
    react-redux,
    react-router-dom,
    react-scripts,
    redux,
    redux-thunk,
    web-vitals;

technologies used on the backend:
    bcrypt,
    bcryptjs,
    body-parser,
    cors,
    dotenv,
    express,
    jsonwebtoken,
    mongoose,
    passport,
    passport-jwt,
    validator;
This simple twitter clone. 
In this clone one can to register any quantity of users, but only one of them is authorized.
One can to change authorized user.
User database model contains in itself them email login password, array Ids of authorized and non 
authorized users following, and array IDs of authorized users followers. One can to find users according his login.
User can to create post. Post database model including in itself user data (his login email),
which created this post, text and ID of post. One can to output all posts of users or posts which was created by users with IDs
in arrays following of models of databases of all registered users. 
