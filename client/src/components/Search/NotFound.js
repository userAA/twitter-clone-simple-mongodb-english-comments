import React from 'react'

//Module, which talk about, that needed user is not founded
const NotFound = () => 
{
    return (
        <h2 style={{textAlign: 'center'}}>User not found</h2>
    )
}

export default NotFound;