import React, {Component} from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreVert from '@material-ui/icons/MoreVert';

import {Link} from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'

import { logoutUser } from '../../actions/authActions';
import SearchForm from '../Search/SearchForm';

//the styles of main page, it via props transfere into classes
const styles = 
{
    root: 
    {
        flexGrow: 1
    },
    logo: 
    {
        color: '#fff',
        fontSize: 30,
        textTransform: 'uppercase'
    },
    space: 
    {
        justifyContent: 'space-between'
    }
} 

//class of main page
class Header extends Component 
{
    constructor (props) 
    {
        super(props);
        this.state = 
        {
            //anchor showing any context menu
            anchorEl: null 
        }
        this.handleLogout = this.handleLogout.bind(this)
    }

    //function showing any context menu
    handleMenu = (event) => { this.setState({anchorEl: event.currentTarget})  }
    
    //function closing any context menu
    handleClose = () => {this.setState({anchorEl: null})}

    //the same function at canceling authorization of an authorized user or at registration of new user
    handleLogout = () => 
    {
        this.setState({anchorEl: null});
        //we removing authorization of an authorized user if it is
        this.props.logoutUser(); 
    }

    render () {
        //we finding from props the classes of styles headering page, 
        //flag of user authorization isAuthenticated or information about user
        const {classes, isAuthenticated, user} = this.props;
        const {anchorEl} = this.state;
        //the flag of opening menus
        let open = Boolean(anchorEl); 

        //menu authorization or registration of user
        const guestLinks = (
            <div>
                {/*Icon menu*/}
                <IconButton
                    aria-owns={open ? 'menu-appbar' : undefined}
                    color="inherit"
                    onClick={this.handleMenu}
                >
                    <MoreVert/>
                </IconButton>
                <Menu
                    id="menu-appbar"
                    open={open}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    anchorEl={anchorEl}
                    onClose={this.handleClose}
                >
                     {/*Opening item menu of authorization user */}
                    <MenuItem onClick={this.handleClose}>
                        <Link to="/login">Login</Link>
                    </MenuItem>
                    {/*Opening item menu of registration of user */}
                    <MenuItem onClick={this.handleLogout}>
                        <Link to="/register">Register</Link>
                    </MenuItem>
                </Menu>
            </div>
        )

        //menu of an authorized user
        const authLinks = isAuthenticated && (
            <div>
                <IconButton
                    aria-owns={open ? 'menu-appbar' : undefined}
                    color="inherit"
                    onClick={this.handleMenu}
                >
                    <AccountCircle/>
                </IconButton>
                <Menu
                    id="menu-appbar"
                    open={open}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    anchorEl={anchorEl}
                    onClose={this.handleClose}
                >
                    {/*We opening item menu of showing posts of authorized user */}
                    <MenuItem onClick={this.handleClose}>
                        <Link to={`/profile/${user._id}`}>Profile</Link>
                    </MenuItem>
                    {/*We opening item menu of canceling authorization of an authorized user  */}
                    <MenuItem>
                        <Link to="/#" onClick={this.handleLogout}>Logout</Link>
                    </MenuItem>
                </Menu>
            </div>
        )

        return (
            <div className={classes.root}>
                <AppBar position="static" style={{backgroundColor: '#4B0082'}}>
                    <Toolbar className={classes.space}>
                        {/*The reference of returning into main page */}
                        <Link to="/" className={classes.logo}>Twit</Link>
                        {/*The window of search posts according to specified user, include non authorized */}
                        <SearchForm/>
                        {/*We showing menus. Depending from value of flags isAuthenticated or menu  
                           authorization and  registration of user or menu of an authorized user */}
                        {isAuthenticated ? authLinks : guestLinks}
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}
 
//закачиваем в props из store через authReducer флаг авторизации пользователя 
//isAuthenticated или информацию о пользователе user
//we uploading into props from store via authReducer the flag of user authorization
//isAuthenticated or information about user user
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user
})

//функцию отмены авторизации пользователя logoutUser оборачиваем в props на выходе
//получаем в mapStateToProps isAuthenticated и user
//the function of canceling user authorization logoutUser wrap in props
//on output receive in mapStateToProps isAuthenticated and user
//c помощью withStyles связываем стили с props
//with help withStyles accociated styles with props
export default connect(mapStateToProps, {logoutUser})(withStyles(styles)(Header));