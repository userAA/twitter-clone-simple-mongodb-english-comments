const router = require('express').Router()
const passport = require('passport')

//finding post model
const Post = require('../models/Post')

//router of creating new post
router.route('/add').post(passport.authenticate('jwt', {session: false}), (req, res) => 
{
    //text of new post of authorized user
    const text = req.body.text.trim()
       
    //we configurating new post of authorized user (text of post, ID and login of authorized user)
    const newPost = new Post(
    {
        user: 
        {
            id: req.user.id,
            login: req.user.login
        },
        text
    })

    //we bringing new post of authorized user in model database of posts on server mobgoDb
    newPost.save().then(post => res.json(post)).catch(err => console.log(err));
})

//router of receiving all posts from all users
router.route('/').get((req,res) => 
{
    //the list of posts we creating such the most recent - first and so on
    Post.find().sort(
    {
        createdAt: -1
    })
    //we sending the list of received posts into frontend
    .then(posts => res.json(posts))
    //arised error at uploading of specified posts
    .catch(err => console.log(err));
})  
    
//router of receiving all posts according user ID from array req.user.following
//and req.user  - total data of authorized user
router.route('/following').get(passport.authenticate('jwt', {session: false}), (req, res) => 
{
    //at first from passport we receiving req.user - all data of authorized user

    //we finding all specified posts
    Post.find(
    {
        'user.id': {$in: req.user.following}
    })   
    //the list user posts with ID userId we creating so the most recent - first and so far
    .sort({createdAt: -1})
    //we sending created list of posts into frontend
    .then(posts => res.json(posts)).catch(err => console.log(err))
})

//router of receiving posts from user with ID req.params.userId
router.route('/:userId').get((req,res) => 
{
    //we finding in database model according to posts on server MongoDb the list of posts with user ID req.params.userId
    Post.find(
    {
        'user.id': req.params.userId
    })
    //the list user posts with ID userId we creating so the most recent - first and so far
    .sort({createdAt: -1})
    //we sending the list of received posts into frontend
    .then(posts => res.json(posts))
    //arised error at uploading of specified posts
    .catch(err => console.log(err))
})

module.exports = router;