const express = require('express');
const dotenv = require('dotenv');

//we finding mongoose module connecting to data base mongodb
const mongoose = require('mongoose');

const bodyParser = require('body-parser');

//we finding cors - analog proxy
const cors = require('cors');

//we finding passport - access module
const passport = require('passport');

//we finding router according to users
const users = require('./routes/users');

//we finding router according to posts
const posts = require('./routes/posts');

//setup environment
dotenv.config();

//we connecting database mongo db 
mongoose.connect(process.env.MONGODB_URL, {useNewUrlParser: true});

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))
//we connecting cors it allows to implement access from frontend to backend and back
app.use(cors())

//we initializing passport
app.use(passport.initialize())
require('./config/passport')(passport)

//we connect router according to users
app.use('/api/users',users);

//we connecting router according to posts
app.use('/api/posts',posts);

//we starting app according to port 5000
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));