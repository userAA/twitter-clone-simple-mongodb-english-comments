import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { connect } from "react-redux"
import { withRouter} from "react-router-dom"

//finding the function of searching profile user data (including his posts) according to his specified login 
import { searchUser } from "../../actions/profileActions"

// page styles of searching profile user data (including his posts) according specified login, they are via props go to classes
const styles = (theme) => ({
    search: {
        position: 'relative',
        borderRadius: theme.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit*3,
            width: 'auto'
        }
    },
    searchIcon: {
        width: theme.spacing.unit*9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200
        }
    }
})

//page search of profile user data (including it posts) according it specified login 
class SearchForm extends Component {
    constructor (props) {
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit (e) {
        const searchData = {
            //user login according to which is being searched on server of profile of total his data (including his posts)
            text: e.target.value 
        }

        //by pressing the key Enter we seeking profile of user data on server (including his posts) according his specified login
        if (e.key === 'Enter') {
            this.props.searchUser(searchData, this.props.history)
        }
    }

    render () {
        const {classes} = this.props;
        return (
            <div className={classes.search}>
                {/*Icon*/}
                <div className={classes.searchIcon}>
                    <SearchIcon/>
                </div>
                {/*Field of entering login user, profile data of which (including all posts) need to find on server*/}
                <InputBase 
                    placeholder="Search user"
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                    }}
                    onKeyPress={this.handleSubmit}
                />
            </div>
        )
    }
}

//search function of profile user data (including his posts) according to specified login we wraping into props
export default connect(null, {searchUser})(withRouter(withStyles(styles)(SearchForm)));