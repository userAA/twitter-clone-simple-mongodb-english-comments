import React , { Component} from 'react'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { Link } from 'react-router-dom'

//the styles of page showing post, they are transfering into classes via props
const styles = 
{
    paper: 
    {
        padding: 10,
        display: 'flex',
        marginTop: 10,
    },
    avatar: 
    {
        minWidth: 10,
        margin: '4px 10px 4px 4px'
    },
    login: 
    {
        marginBottom: 5
    },
    time: 
    {
        marginLeft: 10,
        color: '#bbb',
        fontSize: 14
    }
}

//the class of page showing post
class Post extends Component 
{
    render () 
    {
        //we finding classes of styles of page showing post and itself post from props
        const { classes, post } = this.props;
        return (
            <Paper className={classes.paper}>
                {/*Post avatar*/}
                <div
                    className={classes.avatar}
                    style=
                    {{
                        backgroundColor: `#${post.user.id.slice(post.user.id.length - 3)}`
                    }}
                />
                <div>
                    <h3 className={classes.login}>
                        {/*Switching into profile of user posts with ID post.user.id*/}
                        <Link to={`/profile/${post.user.id}`}>{post.user.login}</Link>  
                        {/*Post creation time*/}                       
                        <span className={classes.time}>{(new Date(post.createdAt)).toLocaleString()}</span>
                    </h3>
                    {/*Text of post */}
                    {post.text}
                </div>
            </Paper>
        )
    }
}

//with help withStyles we linking styles with props
export default withStyles(styles)(Post)