import { 
    //we finding label errors
    GET_ERRORS 
} from "../constants"

const initialState = {}

//reducer errors
const errorReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ERRORS:
            return action.payload
        default:
            return state
    }
}

export default errorReducer;