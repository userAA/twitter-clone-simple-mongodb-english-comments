import React from 'react';
import Grid from '@material-ui/core/Grid';
import Header from './Header';
import Footer from './Footer';

//main page
const Main = ({children}) => {
    return (
        <div>
            {/*Headering module*/}
            <Header/>
            {/*Working modules*/}
            <Grid container justify="center">
                <Grid item xs={12} sm={6} style={{marginTop: 30}}>
                    {children}
                </Grid>
            </Grid>
            {/*Refference into github */}
            <Footer/>
        </div>
    )
}

export default Main;