const Validator = require('validator');

//the function of control data, required for registration new user
module.exports = function(data) 
{
    let errors = {};

    //we checking email of registering user
    if (Validator.isEmpty(data.email)) 
    {
        //there is no email of registering user
        errors.email = 'Email field is required';
    }

    if (!Validator.isEmail(data.email)) 
    {
        //the email of registering user is not correct
        errors.email = 'Email is invalid';
    }

    //we checking login of registering user
    if (Validator.isEmpty(data.login)) 
    {
        //there is no login of registering user
        errors.login = 'Login field is required';
    }

    if (!Validator.isLength(data.login, {min: 4, max: 30})) 
    {
        //login of registering user is not correct
        errors.login = 'Login must between 4 and 30 characters';
    }

    //we checking password of registering user
    if (Validator.isEmpty(data.password)) 
    {
        //there is no the password of registering user
        errors.password = 'Password field is required';
    }

    if (!Validator.isLength(data.password, {min: 6, max: 30})) 
    {
        //the password of registering user is not correct
        errors.password = 'Password must between 6 and 30 characters';
    }

    //we checking the password confirmation of registering user
    if (Validator.isEmpty(data.password2)) 
    {
        //there is no confirmed password of registering user
        errors.password2 = 'Confirm password is required'
    }    

    if (!Validator.equals(data.password, data.password2))
    {
        //the confirmed password of registering user is not correct 
        errors.password2 = 'Password must match'
    }    

    return {
        errors,
        //the condition at that data, needed for registration new user is correct
        isValid: Object.keys(errors).length == 0
    }
}