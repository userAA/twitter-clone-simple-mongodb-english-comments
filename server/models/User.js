const mongoose = require('mongoose')
const Schema = mongoose.Schema;

//we creating database model according to user data
const userSchema = new Schema(
{
    //email user
    email: 
    {
        type: String,
        required: true,
        unique: true
    },
    //login user
    login: 
    {
        type: String,
        required: true
    },
    //password user
    password: 
    {
        type: String
    },
    followers: [],
    following: []
})

//we fixing database model accordding to user data in mongodb service
module.exports = mongoose.model('User', userSchema);