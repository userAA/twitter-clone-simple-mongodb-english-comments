import React, {Component} from 'react'
import Switch from '@material-ui/core/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel'

//we finding the page of adding post of authorized user
import AddPost from './AddPost'

//we finding component of post
import Post from './Post'

import { connect } from 'react-redux'

//we finding the function of receiving posts
import 
{ 
    // getPosts function of receiving posts from all users
    getPosts, 
    //getPostsByFolowingUsers function of receiving posts, ID users of which
    //corespond to user IDs of array following database model of authorized user on server
    getPostsByFolowingUsers 
} from '../../actions/postActions'

//we finding detector of uploading posts
import LoadingPosts from './LoadingPosts'

//the class of pages of list posts
class ListPost extends Component 
{
    constructor (props) 
    {
        super(props)

        this.state = 
        {
            //flag from which functions to load posts from the getPosts function or from the getPostsByFolowingUsers function
            allPosts: true 
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) 
    {
        //we defining flag from which functions to load posts from the getPosts function or from the getPostsByFolowingUsers function
        this.setState({allPosts: event.target.checked})
    }

    componentDidMount() 
    {
        //we uploading all posts at initial time
        this.props.getPosts()
    }

    componentDidUpdate(prevProps, prevState) 
    {
        if (prevState.allPosts !== this.state.allPosts) 
        {
            //depending on the value of the boolean flag this.state.allPosts is determined by
            //which query functions will download posts from the server
            this.state.allPosts ? this.props.getPosts() : this.props.getPostsByFolowingUsers()
        }
    }

    render () 
    {
        //we fiding list from props uploaded
        const {list, loading} = this.props;
        //flag for displaying posts, either all posts or posts with the follow tag
        const {allPosts} = this.state;

        //we fixing all uploaded posts
        const items = list && list.map(el => <Post key={el._id} post={el}/>)

        return (
            <div>
                {/*The page of creating new posts of authorized user  */}
                <AddPost />
                {/*We checking defining value of logic flag this.state.allPosts */}
                <FormControlLabel
                    control = {
                        <Switch checked={allPosts} onChange={this.handleChange}/>
                    }
                    label={allPosts ? 'All posts' : 'From following users'}
                />
                {/*Or show detector of uploading posts or themeselves uploaded posts*/}
                {loading ? <LoadingPosts/> : items}
            </div>
        )
    }
}

const mapStateToProps = (state) => (
{
    //received list of downloading specified posts from store
    list: state.post.list,             
    //flag of detector downloading of specified posts from store
    loading: state.post.loading        
})

//getPosts and getPostsByFolowingUsers functions of request for downloading specified posts we wraping into props

//at output list and loading we receiving in mapStateToProps and functions of request for downloading posts 
//this.props.getPosts and this.props.getPostsByFolowingUsers
export default connect(mapStateToProps, {getPosts, getPostsByFolowingUsers}) (ListPost)