import React, {Component} from "react";
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import { withRouter } from "react-router-dom";
import {connect} from 'react-redux';

//finding the function of request for register user from "../../actions/authActions"
import { registerUser } from "../../actions/authActions";

//the styles of user registration page , they via props transfering into classes
const styles = 
{
    textField: 
    {
        width: '100%',
        marginBottom: 5
    },
    btnBlock: 
    {
        textAlign: 'center',
        marginBottom: 10,
        marginTop: 20
    }
}

//the class of user registration page
class Register extends Component 
{
    constructor (props) 
    {
        super(props);
        //the state of registering user data
        this.state = 
        {
            email: '',
            login: '',
            password: '',
            password2: '',
            errors: {}
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps) 
    {
        if (nextProps.errors) 
        {
            this.setState({errors: nextProps.errors})
        }
    }

    handleChange (e) 
    {
        //we creating one element of data with name e.target.name and value e.target.value  
        //from full known state of data this.state of regestering user
        this.setState({[e.target.name]: e.target.value});
    }

    //the function of register user according it created data
    handleSubmit (e) {
        e.preventDefault();
        //we collecting all data about regestering user
        const userData = {
            email: this.state.email,
            login: this.state.login,
            password: this.state.password,
            password2: this.state.password2
        }
        //we sending request for user registration according total state of it created data
        this.props.registerUser(userData,this.props.history);
    }

    render () 
    {
        //we finding from props the classes of styles of user registration page
        const {classes} = this.props; 
        const {errors}  = this.state;
        return (
            <Paper style={{padding: 15}}>
                <form onSubmit={this.handleSubmit}>
                    {/*The field for setting the e-mail of the registered user */}
                    <TextField
                        type="email"
                        label="Email"
                        name="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        className={classes.textField}
                        helperText={errors.email ? errors.email : ''}
                        error={errors.email ? true : false}
                    />
                    {/*The field for setting the login of the registered user */}
                    <TextField
                        label="Login"
                        type="text"
                        name="login"
                        value={this.state.login}
                        onChange={this.handleChange}
                        className={classes.textField}
                        helperText={errors.login ? errors.login : ''}
                        error={errors.login ? true : false}
                    />
                    {/*The first field for setting the password of the registered user*/}
                    <TextField
                        label="Password"
                        type="password"
                        name="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        className={classes.textField}
                        helperText={errors.password ? errors.password : ''}
                        error={errors.password ? true : false}
                    />
                    {/*The second field for setting the password of the registered user*/}
                    <TextField
                        label="Repeat password"
                        type="password"
                        name="password2"
                        value={this.state.password2}
                        onChange={this.handleChange}
                        className={classes.textField}
                        helperText={errors.password2 ? errors.password2 : ''}
                        error={errors.password2 ? true : false}
                    />
                    <div className={classes.btnBlock}>
                        {/*The button of registration user according it created data */}
                        <Button variant="outlined" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </Paper>
        )
    }
}

const mapStateToProps = (state) => (
{
    //errors which arised at user registration id is being appearing from errorReducer
    errors: state.errors 
})

//request function for user registration registerUser we wraping in props
//at output we receiving mapStateToProps and request for user registration this.props.registerUser
//with help withStyles we accociating styles with props
export default connect(mapStateToProps, {registerUser}) (withRouter(withStyles(styles)(Register)));