import React, {Component} from 'react';
import Paper from '@material-ui/core/Paper'; 
import TextField from '@material-ui/core/TextField'; 
import Button from '@material-ui/core/Button'; 
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

//we finding the function of adding new post
import { addPost } from '../../actions/postActions'

//the styles of page adding new posts, they via props transfer into classes
const styles = 
{
    paper: 
    {
        padding: 8
    },
    textField: 
    {
        width: '100%'
    },
    button: 
    {
        width: '100%',
        marginTop: 20,
        marginBottom: 10,
        backgroundColor: '#800080',
        color: "#fff",
        '&:hover': 
        {
            color: '#800080'
        }
    }
}

//class of page adding of new post
class AddPost extends Component 
{
    constructor (props) 
    {
        super(props)
        this.state = 
        {
            //the text of new post of authorized user
            text: '' 
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange (e) 
    {
        //we creating the text of new post
        this.setState({text: e.target.value})
    }

    handleSubmit (e) 
    {
        e.preventDefault();

        //we creating data of new post of authorized user
        const postData = 
        {
            text: this.state.text
        }

        //we creating new post authorized user according to creating data postData
        this.props.addPost(postData);

        //the text of new post of authorized user clear
        this.setState({text: ''})
    }

    render () 
    {
        //we finding from props classes of styles page of adding new post
        const {classes} = this.props; 
        return (
            <Paper className={classes.paper}>
                {/*Text input field for a new post of an authorized user */}
                <TextField
                    multiline
                    rowsMax="4"
                    label="What's is new?"
                    className={classes.textField}
                    onChange={this.handleChange}
                    value={this.state.text}
                />
                {/*The button of creating new post of authorized user */}
                <Button 
                    variant="outlined" 
                    className={classes.button}
                    onClick={this.handleSubmit}
                >
                    Send
                </Button>
            </Paper>
        )
    }
}

//the function of request for creating new post of authorized user addPost we wraping into props
//with help withStyles we accociating styles with props
export default connect(null, {addPost})(withStyles(styles)(AddPost));