import axios from 'axios'
import 
{ 
    //finding the label of errors
    GET_ERRORS, 
    //finding the label of sending into storage store the data about authorized user
    SET_CURRENT_USER 
} from "../constants";
import setAuthHeader from '../utils/setAuthHeader';

//the request of function for user authorization 
export const loginUser = (userData)  => dispatch => 
{
    //request into server according to authorization of registered user
    axios.post('http://localhost:3001/api/users/login',userData).then(res => 
    {
        //in the case of success we finding token of registered user authorization 
        const {token} = res.data;
        //we writing this token into localStorage via record jwtToken
        localStorage.setItem('jwtToken',token);
        //Creating state.auth based on token in store according to authReducer 
        setAuthHeader(token);
        //we receiving full information about authorized user
        dispatch(getCurrentUser());
    })
    //we transmiting into errorReducer in the case of poor authorization
    .catch(err => 
    {
        dispatch(
        {
            type: GET_ERRORS,
            payload: err.response.data
        })
    })
}

//request of function for registration user
export const registerUser = (userData,history) => dispatch => 
{
    //based on axios making up post request into server according to registration user
    axios.post('http://localhost:3001/api/users/register',userData)
    //in the case of success we transmiting into page of authorization user /login 
    .then(res => history.push('/login'))
    //transmiting into errorReducer in the case of poor registration
    .catch(err => dispatch(
    {
        type: GET_ERRORS,
        payload: err.response.data
    }))
}

//the function of receiving from server information about authorized user
export const getCurrentUser = () => dispatch => 
{
    //we receiving information about authorized user from server and we sending it into storage store
    axios.get('http://localhost:3001/api/users').then(res => dispatch(setCurrentUser(res.data)))
}

//the function of sending information about authorized user in storage store
export const setCurrentUser = (data) => 
{
    return {
        type: SET_CURRENT_USER,
        payload: data
    }
}

//the function of cancelling user registration
export const logoutUser = () => dispatch => 
{
    //we canceling user authorization according to jwtToken
    localStorage.removeItem('jwtToken')
    //Creating empty state.auth based on token in store according to authReducer 
    setAuthHeader()
    //uploading into storage the empty information about user
    dispatch(setCurrentUser())    
}