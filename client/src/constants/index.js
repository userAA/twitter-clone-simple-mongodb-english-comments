//label errors
export const GET_ERRORS = 'GET_ERRORS'            
//label of sending into storage store the data about authorized user
export const SET_CURRENT_USER = 'SET_CURRENT_USER'
//label starting up of detector uploading posts
export const LOADING_POSTS = 'LOADING_POSTS'       
//label of request for receiving of all posts from server 
export const GET_POSTS = 'GET_POSTS'               
//label request for adding of new posts into storage store
export const ADD_POST = 'ADD_POST'                 
//label of starting up uploading detector of specified profile data 
export const LOAD_PROFILE = 'LOAD_PROFILE'        
//label of request for receiving profile user data according to his specified ID userId
export const GET_PROFILE = 'GET_PROFILE'          
//label of adding user IDs
export const FOLLOW = 'FOLLOW'                     
//label of removing user IDs
export const UNFOLLOW = 'UNFOLLOW'                 
